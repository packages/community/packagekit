# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Christian Hesse <mail@eworm.de>
# Contributor: Jonathan Conder <jonno.conder@gmail.com>

pkgbase='packagekit'
pkgname=('packagekit' 'libpackagekit-glib')
pkgver=1.2.8
pkgrel=7
pkgdesc='A system designed to make installation and updates of packages easier'
arch=('x86_64' 'aarch64')
url='https://www.freedesktop.org/software/PackageKit/'
license=('GPL-2.0-or-later')
makedepends=('polkit' 'sqlite' 'gobject-introspection' 'intltool'
             'bash-completion' 'vala' 'meson')
options=('!emptydirs')
validpgpkeys=('163EB50119225DB3DF8F49EA17ACBA8DFA970E17'        # Richard Hughes <richard@hughsie.com>
              'EC60AABDF42AAE8FB062640480858FA38F62AF74')       # Kalev Lember <klember@redhat.com>
source=("https://www.freedesktop.org/software/PackageKit/releases/PackageKit-${pkgver}.tar.xz"
        '0001-alpm-add-compatibility-with-libalpm-14.patch'
        '0019-Fix-SyncFirst-crash.patch')
sha256sums=('d834250a0f121483027db2cfd93e8b2cba8dc48ff854395bfd88aa9511898be4'
            '2a75223614fceffe12e2c5cbc8534a927be583f798cd7a5fbd41348b464ecbf7'
            '4c3044f421192828273c3f5f8edb8fcc3503e9621315bc7cc7cceaf3b1932633')

prepare() {
  shopt -s nullglob

  for _patch in *.patch; do
    patch -d PackageKit-$pkgver -p1 < $_patch
  done
}

build() {
        local _meson_options=(
                -Dcron=false
                -Dgstreamer_plugin=false
                -Dgtk_doc=false
                -Dgtk_module=false
                -Dpackaging_backend=alpm
                -Dsystemd=true
        )

        arch-meson "PackageKit-$pkgver" build "${_meson_options[@]}"

        meson compile -C build
}

package_packagekit() {
        depends=('libpackagekit-glib' 'pacman>=6.0.0' 'polkit' 'sqlite')
        optdepends=('bash-completion: command completion in bash')
        backup=('var/lib/PackageKit/transactions.db'
                'etc/PackageKit/alpm.d/pacman.conf'
                'etc/PackageKit/alpm.d/repos.list')

        meson install -C build --destdir "$pkgdir"

        # move away for libpackagekit-glib
        mkdir -p libpackagekit/usr/{lib,share}
        mv "$pkgdir"/usr/include/ libpackagekit/usr/
        mv "$pkgdir"/usr/lib/{girepository-1.0,libpackagekit-glib2.so*,pkgconfig} libpackagekit/usr/lib/
        mv "$pkgdir"/usr/share/{gir-1.0,vala}/ libpackagekit/usr/share/
}

package_libpackagekit-glib() {
        pkgdesc='GLib library for accessing PackageKit'
        depends=('glib2')
        provides=('libpackagekit-glib2.so')

        mv libpackagekit/usr/ "$pkgdir"/
}

